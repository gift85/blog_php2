<?php

namespace Controller;

use Core\Exceptions\E404;
use Model\{Page, System, Users};
use Controller\Users\User;
use Core\Validation;
/*разделить на смысловые классы*/
class Blog extends User{

    public function action_index(){
        $page = Page::instance();
        $titles = $page->get_index();

        /*с такой проверкой $message всегда будет пуст*/
        if(is_null($titles)){
            throw new E404('DB is empty');
        }
        /*errmsg 'Can not show articles from DB.');*/
        if(!empty($_SESSION['msg'])){
            $message = $_SESSION['msg'];
            unset($_SESSION['msg']);
            $message_class = 'info';
        }
        else{
            $message = '';
            $message_class = '';
        }

        $inner_vars = compact('message_class', 'message', 'titles');
        $page_content = System::make_template("blog.php", $inner_vars);

        if($this->user_id){
            $menu[] = System::menu_item('blog/logout', 'Logout');
            $menu[] = System::menu_item('blog/add', 'Add article');
        }
        else{
            $menu[] = System::menu_item('blog/login', 'Login');
        }

        $page_title = 'Blog';

        $this->page_vars += compact('menu', 'page_content', 'page_title');
    }

    public function action_show():void{
        $title_id = System::check_inputs($this->params[2]);
        System::redirect_if_true(empty($title_id));

        $page = Page::instance();
        $article = $page->get_article($title_id)[0];

        /*с такой проверкой $message всегда будет пуст*/
        if(is_null($article)){
            throw new E404('Article not found!');
        }
        /*errmsg 'Can not show article from DB.');*/

        if(!empty($_SESSION['msg'])){
            $message = $_SESSION['msg'];
            unset($_SESSION['msg']);
            $message_class = 'info';
        }
        else{
            $message = '';
            $message_class = '';
        }
        $title = $article['title'];
        $content = $article['content'];
        $created = strftime('%d.%m.%Y %R', strtotime($article['created']));
        $author = $article['user'];

        $inner_vars = compact('message_class', 'message', 'title', 'content', 'created', 'author');
        $page_content = System::make_template("show.php", $inner_vars);

        $menu[] = System::menu_item('', 'Back to main');
        if($this->user_id == $article['user_id']){
            $menu[] = System::menu_item("blog/edit/$title_id", 'Edit article');
            $menu[] = System::menu_item("blog/remove/$title_id", 'Delete article');

        }
        $page_title = "Blog: $title";

        $this->page_vars += compact('menu', 'page_content', 'page_title');
    }

    public function action_remove():void{
        $title_id = System::check_inputs($this->params[2]);
        System::redirect_if_true(!$this->user_id);

        $page = Page::instance();
        $remove_result = $page->remove($title_id);

        /*с такой проверкой $message всегда будет пуст
        нужно создать другую оишбку*/
        if($remove_result === 0){
            $_SESSION['msg'] = 'Nothing was removed';
            System::redirect_if_true(true, "blog/show/$title_id");
            //throw new E404('Nothing was removed');
        }
        $_SESSION['msg'] = "Article $title_id removed!";
        /*errmsg 'Can not delete article from DB.');*/
        System::redirect_if_true(true);

    }

    public function action_add():void{
        System::redirect_if_true(!$this->user_id);
        $title = $_POST['title'] ?? null;
        $content = $_POST['content'] ?? null;
        $message_class = 'error';
        $message = '';

        if((is_null($title) || is_null($content))){
            $message = 'Write new content';
            $message_class = 'info';
        }
        else{
            $page = Page::instance();
            $data = compact('title', 'content');
            $valid = new Validation($data, $page->validation_rules());
            $valid->execute();

            if($valid->good()){
                $params = $valid->clean_data();
                $params['user_id'] = $this->user_id;
                $add_to_db_status = $page->add($params);

                /*с такой проверкой $message всегда будет пуст
                нужно создать другую оишбку*/
                if($add_to_db_status === 0){
                    throw new E404('Nothing was added');
                }
                /*errmsg 'Can not add this article to DB.');*/
                $_SESSION['msg'] = "Article $title added!";
                System::redirect_if_true(true);
            }
            else{
                foreach($valid->errors() as $value){
                    $message .= $value . ' ';
                }
            }
        }

        $inner_vars = compact('message_class', 'message', 'title', 'content');
        $page_content = System::make_template("add.php", $inner_vars);
        $menu[] = System::menu_item('', 'Back to main');
        $page_title = 'Blog: New article';

        $this->page_vars += compact('menu', 'page_content', 'page_title');
    }

    public function action_edit():void{
        System::redirect_if_true(!$this->user_id);

        //System::redirect_if_true(!isset($this->params[2]));
        $title_id = System::check_inputs($this->params[2]);

        $get_ok = false;
        $message_class = 'error';
        $message = '';
        $page = Page::instance();
        $data = compact('title_id');
        $valid = new Validation($data, $page->validation_rules());
        $valid->execute();
        if($valid->good()){
            $article = $page->get_article($title_id)[0];
            /*с такой проверкой $message всегда будет пуст
               нужно создать другую оишбку*/
            if(is_null($article)){
                throw new E404('Article not found!');
            }
            System::redirect_if_true($this->user_id != $article['user_id']);
            $message = '';
            $title = $article['title'];
            $content = $article['content'];
            $message_class = '';
            $get_ok = true;
        }
        else{
            foreach($valid->errors() as $value){
                $message .= $value;
            }
        }

        $new_title = $_POST['edit_title'] ?? null;
        $new_content = $_POST['edit_content'] ?? null;

        /*if 'get check flag' good and article was edited*/
        if(!is_null($new_title) && !is_null($new_content) && $valid->good()){
            $page = Page::instance();
            $set = [
                "title" => $new_title,
                "content" => $new_content
            ];
            $valid2 = new Validation($set, $page->validation_rules());
            $valid2->execute();
            if($valid2->good()){
                $set = $valid2->clean_data();
                $new_article_adding = $page->update($title_id, $set);

                /*с такой проверкой $message всегда будет пуст
               нужно создать другую оишбку*/
                if($new_article_adding === 0){
                    throw new E404('Nothing was updated');
                }
                /*errmsg 'Can not update article from DB.');*/
                $_SESSION['msg'] = "Article $title_id updated!";
                System::redirect_if_true(true, "blog/show/$title_id");
            }
            else{
                $message_class = 'error';
                foreach($valid->errors() as $value){
                    $message .= $value;
                }
            }
        }

        $inner_vars = compact('message_class', 'message', 'title', 'content', 'get_ok');
        $page_content = System::make_template("edit.php", $inner_vars);
        $menu[] = System::menu_item("blog/show/$title_id", 'Return');
        $menu[] = System::menu_item('', 'Back to main');
        $page_title = 'Blog: Edit article';

        $this->page_vars += compact('menu', 'page_content', 'page_title');
    }

    public function action_logout():void{
        if(isset($this->params[1])){
            if(isset($_SESSION['auth'])){
                $login = $_SESSION['auth'];
                unset($_SESSION['auth']);
                unset($_SESSION[$login]);
            }
            setcookie('login', '', time() - 3600);
        }
        System::redirect_if_true(true);
    }

    public function action_login():void{
        $message = 'Welcome';
        $message_class = 'info';

        if(count($_POST) > 0){
            $login = trim($_POST['login']);
            $password = trim($_POST['password']);

            $page = Users::instance();
            $user_data= $page->get_user($login);
            /*заменить на другую ошибку*/
            if(is_null($user_data)){
                throw new E404('User not found!');
            }
            /*errmsg 'User not found'*/

            if(md5($password) === $user_data['password']){
                $_SESSION['auth'] = $user_data['user_id'];
                if(isset($_POST['save_me'])){
                    $mark = md5(time() . md5($password));
                    $_SESSION[$user_data['user_id']] = $mark;
                    setcookie($user_data['user_id'], $mark, time() + 3600 * 24 * 7);
                    //setcookie('login', $login, time() + 3600 * 24 * 7);
                }
                else{
                    setcookie($login, '', time());
                }
                System::redirect_if_true(true);
            }
            else{
                $message = 'Access denied';
                $message_class = 'error';
            }
        }

        /*message for authorized users*/
        elseif($this->user_id){
            $message = "You are already authorized.<br> If you want to log out push <a href='" . ROOT . "blog/login/logout'>HERE</a>";
            $message_class = 'error';
        }

        $is_authorized = !empty($this->user_id);
        $inner_vars = compact('message_class', 'message', 'is_authorized');
        $page_content = System::make_template("login.php", $inner_vars);
        $menu[] = System::menu_item('', 'Back to main');
        $page_title = 'Blog: Authorization';

        $this->page_vars += compact('menu', 'page_content', 'page_title');
    }

    public function action_register():void{
        System::redirect_if_true(true);
    }
}






