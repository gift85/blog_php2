<?php

namespace Controller;

use Model\{Page, System};
use Controller\Users\User;
use Core\Validation;
/*переделать под выдергивание юзеров*/
class Bylog extends User{

    public function action_index(){
        $page = Page::instance();
        $titles = $page->get_index();

        /*errmsg 'Can not show articles from DB.');*/
        /*catch exception*/
        $message = $page->check_db_result($titles, 'DB is empty');
        $message_class = empty($message) ? '' : 'error';

        $inner_vars = compact('message_class', 'message', 'titles');
        $page_content = System::make_template("blog.php", $inner_vars);

        if($this->user_id){
            $menu[] = System::menu_item('blog/logout', 'Logout');
            $menu[] = System::menu_item('blog/add', 'Add article');
        }
        else{
            $menu[] = System::menu_item('blog/login', 'Login');
        }

        $page_title = 'Blog';

        $this->page_vars += compact('menu', 'page_content', 'page_title');
    }

    public function action_show():void{
        $title_id = System::check_inputs($this->params[2]);
        System::redirect_if_true(empty($title_id));

        $page = Page::instance();
        $article = $page->get_article($title_id)[0];
        //var_dump($article);

        /*errmsg 'Can not show article from DB.');*/
        $message = $page->check_db_result($article, 'Article not found!');

        $page_title = 'Blog';
        $message_class = 'error';

        if(empty($message)){
            $title = $article['title'];
            $content = $article['content'];
            $created = strftime('%d.%m.%Y %R', strtotime($article['created']));
            $author = $article['user'];
            $message_class = '';
        }
        else{
            $page_title .= ': error';
        }

        $inner_vars = compact('message_class', 'message', 'title', 'content', 'created', 'author');
        $page_content = System::make_template("show.php", $inner_vars);

        $menu[] = System::menu_item('', 'Back to main');
        if(empty($message) && $this->user_id == $article['user_id']){
            $menu[] = System::menu_item("blog/edit/$title_id", 'Edit article');
            $menu[] = System::menu_item("blog/remove/$title_id", 'Delete article');
            $page_title .= ": $title";
        }

        $this->page_vars += compact('menu', 'page_content', 'page_title');
    }

    public function action_remove():void{
        $title_id = System::check_inputs($this->params[2]);
        System::redirect_if_true(!$this->user_id);

        $page = Page::instance();
        $remove_result = $page->remove($title_id);
        /*errmsg 'Can not delete article from DB.');*/
        $message = $page->check_db_result($remove_result, 'Nothing was removed');
        if(empty($message)){
            System::redirect_if_true(true);
        }
        System::redirect_if_true(true, "blog/show/$title_id");
        /**/
    }

    public function action_add():void{
        System::redirect_if_true(!$this->user_id);
        //var_dump($this->user_id);
        $title = $_POST['title'] ?? null;
        $content = $_POST['content'] ?? null;
        $message_class = 'error';
        $message = '';

        if((is_null($title) || is_null($content))){
            $message = 'Write new content';
            $message_class = 'info';
        }
        else{
            $page = Page::instance();
            $data = compact('title', 'content');
            //var_dump($data);
            $valid = new Validation($data, $page->validation_rules());
            $valid->execute();

            if($valid->good()){
                $params = $valid->clean_data();
                $params['user_id'] = $this->user_id;
                //var_dump($params);
                $add_to_db_status = $page->add($params);

                /*errmsg 'Can not add this article to DB.');*/
                $message = $page->check_db_result($add_to_db_status, 'Nothing was added');
                if(empty($message)){
                    System::redirect_if_true(true);
                }
            }
            else{
                foreach($valid->errors() as $value){
                    $message .= $value . ' ';
                }
            }
        }

        $inner_vars = compact('message_class', 'message', 'title', 'content');
        $page_content = System::make_template("add.php", $inner_vars);
        $menu[] = System::menu_item('', 'Back to main');
        $page_title = 'Blog: New article';

        $this->page_vars += compact('menu', 'page_content', 'page_title');
    }

    public function action_edit():void{
        System::redirect_if_true(!$this->user_id);

        System::redirect_if_true(!isset($this->params[2]));
        $title_id = System::check_inputs($this->params[2]);

        $get_ok = false;
        $message_class = 'error';
        $message = '';
        $page = Page::instance();
        $data = compact('title_id');
        $valid = new Validation($data, $page->validation_rules());
        $valid->execute();
        if($valid->good()){
            /*use funtion to get data and store result to variable*/
            $article = $page->get_article($title_id);
            System::redirect_if_true($this->user_id != $article['user_id']);

            $message = $page->check_db_result($article, 'Article not found!');

            /*if all good*/
            if(empty($message)){
                $title = $article['title'];
                $content = $article['content'];
                $message_class = '';

                /*get check flag*/
                //$get_ok = true;
            }
        }
        else{
            foreach($valid->errors() as $value){
                $message .= $value;
            }
        }

        $new_title = $_POST['edit_title'] ?? null;
        $new_content = $_POST['edit_content'] ?? null;

        /*if 'get check flag' good and article was edited*/
        if(!is_null($new_title) && !is_null($new_content) && $valid->good()){
            $page = Page::instance();
            $set = [
                "title" => $new_title,
                "content" => $new_content
            ];
            $valid2 = new Validation($set, $page->validation_rules());
            $valid2->execute();
            if($valid2->good()){
                /*use funtion to update data and store result to variable*/
                $set = $valid2->clean_data();
                //var_dump($set);
                $new_article_adding = $page->update($title_id, $set);

                /*errmsg 'Can not update article from DB.');*/
                $message = $page->check_db_result($new_article_adding, 'Nothing was updated');
                if(empty($message)){
                    System::redirect_if_true(true, "blog/show/$title_id");
                }
            }
            else{
                $message_class = 'error';
                foreach($valid->errors() as $value){
                    $message .= $value;
                }
            }
        }

        $inner_vars = compact('message_class', 'message', 'title', 'content', 'get_ok');
        $page_content = System::make_template("edit.php", $inner_vars);
        $menu[] = System::menu_item("blog/show/$title_id", 'Return');
        $menu[] = System::menu_item('', 'Back to main');
        $page_title = 'Blog: Edit article';

        $this->page_vars += compact('menu', 'page_content', 'page_title');
    }

    public function action_logout():void{
        if(isset($this->params[1])){
            if(isset($_SESSION['auth'])){
                $login = $_SESSION['auth'];
                unset($_SESSION['auth']);
                unset($_SESSION[$login]);
            }
            setcookie('login', '', time() - 3600);
            //setcookie('password', '', time() - 3600);
        }
        System::redirect_if_true(true);
    }

    public function action_login():void{
        $message = 'Welcome';
        $message_class = 'info';

        if(count($_POST) > 0){
            $login = trim($_POST['login']);
            $password = trim($_POST['password']);

            $page = User::instance();
            $user_pass = $page->get_user($login);
            /*errmsg 'User not found', true);*/

            if(md5($password) === $user_pass['password']){
                $_SESSION['auth'] = $user_pass['user_id'];
                if(isset($_POST['save_me'])){
                    $mark = md5(time() . md5($password));
                    $_SESSION[$user_pass['user_id']] = $mark;
                    setcookie($user_pass['user_id'], $mark, time() + 3600 * 24 * 7);
                    //setcookie('login', $login, time() + 3600 * 24 * 7);
                    //setcookie('password', md5($password), time() + 3600 * 24 * 7);
                }
                else{
                    setcookie($login, '', time());
                }
                System::redirect_if_true(true);
            }
            else{
                $message = 'Access denied';
                $message_class = 'error';
            }
        }

        /*message for authorized users*/
        elseif($this->user_id){
            $message = "You are already authorized.<br> If you want to log out push <a href='" . ROOT . "blog/login/logout'>HERE</a>";
            $message_class = 'error';
        }

        $is_authorized = !empty($this->user_id);
        $inner_vars = compact('message_class', 'message', 'is_authorized');
        $page_content = System::make_template("login.php", $inner_vars);
        $menu[] = System::menu_item('', 'Back to main');
        $page_title = 'Blog: Authorization';

        $this->page_vars += compact('menu', 'page_content', 'page_title');
    }

    public function action_register():void{
        System::redirect_if_true(true);
    }

    public function action_404():void{
        $page_title = 'Error';
        $page_content = '<h1>Sorry, page not found.</h1>You get lost.';
        $menu[] = System::menu_item('', 'Back to main');

        $this->page_vars += compact('menu', 'page_content', 'page_title');
    }
}






