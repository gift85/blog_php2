<?php

namespace Controller;

use Model\System;
use Controller\Users\User;

class Pages extends User{

    public function action_404():void{
        $page_title = 'Error';
        $page_content = '<h1>Sorry, page not found.</h1>You get lost.';
        $menu[] = System::menu_item('', 'Back to main');

        $this->page_vars += compact('menu', 'page_content', 'page_title');
    }
}
