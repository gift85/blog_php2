<?php

namespace Controller\Admin;

use Model\System;
use Controller\Main;

abstract class Admin extends Main{
    protected $title;
    protected $content;

    public function __construct(){
        /*parent::__construct();*/

        /* всех неавторизованных выкидывает на логин */

        $this->title = 'Blog: ';
        $this->content = '';
    }

    public function render():string{
        $html = System::make_template('admins/v_main.php', [
            'title' => $this->title,
            'content' => $this->content
         ]);

        return $html;
    }
}
