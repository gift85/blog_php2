<?php
/*declare(strict_types=1);*//*почему то не работает*/
require_once('autoloader.php');

use Core\Exceptions\E404;

define('ROOT', '/');
define('CHARSET', 'UTF-8');/*not used yet*/
define('LOGS_PATH', 'src/logs/');
define('DEV_MODE', true);/*not used yet*/

$params = explode('/', $_GET['q']);

if(end($params) === ''){
    array_pop($params);
}

$param0 = $params[0] ?? 'blog';
$controllers = ['blog'];

if(!in_array($param0, $controllers)){
    $param0 = 'blog';
    $params[1] = '404';
}

$action = 'action_' . ($params[1] ?? 'index');

$controller_name = 'controller\\' . ucfirst($param0);

try{
    $controller = new $controller_name;
    $controller->load_params($params);
    $controller->set_theme($_GET['theme'] ?? '');
    $controller->$action();
    echo $controller;
}
catch(E404 $e){
    /*добавить отображение ошибки?*/
    $controller = new controller\Pages;
    //$controller->load_params($params);
    $controller->set_theme($_GET['theme'] ?? '');
    $controller->action_404();
    echo $controller;
}
catch(Exception $e){
    echo '<pre>';
    echo $e;
    echo '<pre>';
}
