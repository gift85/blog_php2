<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title><?=$page_title?></title>
    <link rel="stylesheet" type="text/css" href="<?=ROOT?>src\css\style<?=$theme_num?>.css">
</head>
<body>
    <div id="choose_theme">
        <a class="theme<?=$theme_num == '1' ? ' active' : ''?>" href="?theme=1">Theme 1</a>
        <a class="theme<?=$theme_num == '2' ? ' active' : ''?>" href="?theme=2">Theme 2</a>
    </div>

    <div id="wrapper">
        <div id="page_title">
            <?=$page_title?>
        </div>

        <div class="menu">
            <?foreach($menu as $item):?>
                <a href="<?=ROOT?><?=$item['href']?>"><?=$item['text']?></a>
            <?endforeach?>
        </div>
        <hr>
        <?=$page_content?>
    </div>
</body>
</html>
