<div class="message <?=$message_class?>">
    <?=$message?>
</div>
<?if(empty($message) or $message_class == 'info'):?>
<div class="content">
    <div class="title">
        <?=$title?>
    </div>
    <br>
    <div class="article_content">
        <?=nl2br($content)?>
    </div>
</div>
    <hr>
    Created: <?=$created ?? ''?> Author: <?=$author ?? ''?>
<?endif?>
