<div class="message <?=$message_class?>">
    <?=$message?>
</div>
<?if(empty($message) or $message_class == 'info'):?>
    <div class="content articles">
        <ul>
        <?foreach($titles as $title):?>
            <li><a href="<?=ROOT?>blog/show/<?=$title['article_id']?>">News <?=$title['title']?></a></li>
        <?endforeach?>
        </ul>
    </div>
<?endif?>
