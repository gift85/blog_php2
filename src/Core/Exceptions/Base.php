<?php

namespace Core\Exceptions;

use Exception;

class Base extends Exception{
    protected $path = LOGS_PATH;

    public function __construct($message = "", $code = 0, Exception $previous = null){
        parent::__construct($message, $code, $previous);

        $record = "\n " . date("Y-m-d H:i:m:s") . " " . $_SERVER['REMOTE_ADDR'] . "\n" . $this . "\n==================================================";
        //var_dump($_SERVER);
        file_put_contents($this->path . date("Y-m-d"), $record, FILE_APPEND);
    }
}
