<?php

namespace Core\Traits;

/**
 * Class Singleton
 * @package Core\Traits
 */
trait Singleton{
    protected static $singleton_instance;

    public static function instance():self{

        if(is_null(static::$singleton_instance)){
            static::$singleton_instance = new static();
        }
        return static::$singleton_instance;
    }

    protected function __construct(){

    }
    protected function __clone(){

    }
    protected function __wakeup(){

    }
    protected function __sleep(){

    }
}
