<?php

namespace Model;

class System{
    /**
     * @param string $template template name
     * @param array $params assoc array with params
     * @param string $path path to template, default 'view/'
     * @return string template as string
     */
    public static function make_template(string $template, array $params = [], string $path = 'src/view/'):string{
        // var_dump($params);
        extract($params);
        ob_start();
        include $path . $template;
        return ob_get_clean();
    }

    /**
     * if param is true - redirect to index or where you want
     * @param bool $param true condition to redirect
     * @param string $where where to redirect, default 'index.php'
     */
    public static function redirect_if_true(bool $param, string $where = ''):void{
        if($param === true){
            header("Location: " . ROOT . $where);
            exit();
        }
    }

    /**
     * trim and htmlspecialchars
     * @param string $param some parameter
     * @return string
     */
    public static function check_inputs(string $param):string{
        return htmlspecialchars(trim($param));
    }

    public static function theme_check(string $theme_num):string{
        if(empty($theme_num)){
            if(!isset($_COOKIE['theme'])){
                setcookie('theme', '1', time() + 3600 * 24 * 7, ROOT);
                return '1';
            }
            else{
                return $_COOKIE['theme'];
            }
        }
        else{
            setcookie('theme', $theme_num, time() + 3600 * 24 * 7, ROOT);
            return $theme_num;
        }
    }

    public static function auth_check(){
        /*session_start(['read_and_close' => true]);*/
        session_start();
        //var_dump($_SESSION['auth'] ?? 'nothing');
        /*ждем нормальной реализации*/
        return $_SESSION['auth'] ?? false;
    }

    public static function menu_item(string $href, string $text):array{
        return compact('href', 'text');
    }
}
