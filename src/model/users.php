<?php

namespace Model;

use Core\Traits\Singleton;
use Core\Model;

class Users extends Model{
    use Singleton;

    protected function __construct(){
        parent::__construct();
        $this->table = 'users';
        $this->primary_key = 'user_id';
    }

    public function validation_rules(){
        return [
            'fields' => ['user_id', 'name', 'password'],
            'not_empty' => ['user_id', 'name', 'password']
        ];
    }

    public function get_user(string $name):array{
        $sql = "SELECT user_id, password FROM users WHERE name = :name";
        $params = compact('name');

        return $this->db->select($sql, $params);
    }
}
